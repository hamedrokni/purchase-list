import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_n/main.dart';
import 'package:flutter_app_n/widgets/tasks_Lists.dart';

class MyTask extends StatelessWidget{
  int i=0;

@override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.amberAccent ,
floatingActionButton: FloatingActionButton(
  backgroundColor: Colors.amberAccent ,
  child: Icon(
        Icons.add),
  onPressed: (){

    i++;
    main().setState(() {

    });
  },
),

body: Column(
  children: <Widget>[
          Container(
        padding: EdgeInsets.only(top:60.0,bottom: 30.0),
        child:Column(
          crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          CircleAvatar(child: Icon(Icons.list,size: 40.0,color: Colors.amberAccent,),
          backgroundColor: Colors.white,
            radius: 40.0,
          ),
          SizedBox(
            height: 10.0,
          ),
          Text('list To Do!',
            style:TextStyle(color:Colors.black,
                fontSize: 40.0,
                fontWeight:FontWeight.w700,
            )
          ),
          Text('$i Taxt',
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
          ),
          ),
        ],
      ),),
      Expanded(
        child:   Container(padding: EdgeInsets.symmetric(horizontal: 20.0),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(topLeft: Radius.circular(20.0),topRight: Radius.circular(20.0)
              )
          ),
          child:  TasksList(),
        ),
      ),
  ],

)
      ),
    );
  }
}


